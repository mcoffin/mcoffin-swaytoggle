import { ChildProcess, spawn } from 'child_process';
import { Readable, Writable } from 'stream';
import { awaitTerminationChecked } from './process-utils';
import { streamToString } from './stream-utils';
import Lazy from 'lazy.js';
import _ from 'lodash';

async function writeDmenu(s: Writable, options: string[]): Promise<void> {
    return new Promise((resolve, reject) => {
        options.forEach((opt) => {
            s.write(Buffer.from(`${opt}\n`));
        });
        s.end(Buffer.from(''));
    });
}

export interface WofiOptions {
    conf?: string;
    prompt?: string;
    style?: string;
    'allow-images'?: string;
    'allow-markup'?: string;
    width?: number;
    height?: number;
}

interface ArgsFormat {
    plusSeparator?: boolean;
    aliases?: Record<string, string>;
}

function optionsToArgs(options?: Record<string, any>, format?: ArgsFormat): string[] {
    if (!options) {
        return [];
    }
    function getAsString(key: string): string {
        const short = format?.aliases?.[key];
        if (short) {
            return short;
        }
        return `--${key}`;
    }
    return Lazy(options)
        .pairs()
        .filter(([k, v]) => v !== undefined && v !== null)
        .map(([k, v]) => {
            if (_.isBoolean(v) && v) {
                return [getAsString(k)];
            }
            if (format?.plusSeparator) {
                return [`--${k}=${v}`];
            } else {
                return [getAsString(k), `${v}`];
            }
        })
        .flatten()
        .toArray();
}

function wofiOptionsToArgs(options?: Record<string, any>): string[] {
    return optionsToArgs(options, {
        aliases: {
            'allow-images': '-I',
            'allow-markup': '-m',
            'conf': '-c',
            'prompt': '-p',
        },
    });
}

export async function wofi(options: string[], wofiOptions?: WofiOptions): Promise<string> {
    const child = spawn('wofi', ['--dmenu'].concat(wofiOptionsToArgs(wofiOptions)), { stdio: ['pipe', 'pipe', 'inherit'] });
    const readOutput = streamToString(child.stdout, 'utf8');
    writeDmenu(child.stdin, options);
    await awaitTerminationChecked(child, 'wofi');
    const output = await readOutput;
    return output.trim();
}

export async function chooser(command: string, options: string[]): Promise<string> {
    const child = spawn(command, [], { stdio: ['pipe', 'pipe', 'inherit'], shell: true });
    const readOutput = streamToString(child.stdout, 'utf8');
    writeDmenu(child.stdin, options);
    await awaitTerminationChecked(child, command);
    return await readOutput.then((s) => s.trim());
}

export const bemenu = (options: string[]) => chooser('bmenu', options);
export const dmenu = (options: string[]) => chooser('dmenu', options);
