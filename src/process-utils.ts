import _ from 'lodash';
import { ChildProcess, SpawnOptions, spawn } from 'child_process';

export class ExitStatusError extends Error {
    status: string | number;
    processName?: string;
    constructor(status: string | number, name?: string) {
        super(`${name || 'child process'} exited with failure status: ${status}`);
        this.status = status;
        this.processName = name;
    }
}

export function awaitTermination(child: ChildProcess): Promise<string | number> {
    return new Promise((resolve, reject) => {
        child.on('exit', (code, signal) => {
            if (code !== undefined && code !== null) {
                resolve(code);
            } else {
                resolve(signal as string);
            }
        });
    });
}

function isSuccess(status: string | number): boolean {
    return _.isNumber(status) && status === 0;
}

export async function awaitTerminationChecked(child: ChildProcess, name?: string): Promise<ChildProcess> {
    const status = await awaitTermination(child);
    if (!isSuccess(status)) {
        throw new ExitStatusError(status, name);
    }
    return child;
}

export function runSimpleCommand(command: string, args: string[] = [], opts: Omit<SpawnOptions, 'stdio'> = {}): Promise<ChildProcess> {
    const child = spawn(command, args, { ...opts, stdio: ['ignore', 'inherit', 'inherit'] });
    return awaitTerminationChecked(child, command);
}
