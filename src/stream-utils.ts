import { Readable } from 'stream';

export function streamToString(s: Readable, encodingOption?: BufferEncoding): Promise<string> {
    if (encodingOption) {
        s.setEncoding(encodingOption);
    }
    const encoding = encodingOption || 'utf8';
    return new Promise((resolve, reject) => {
        let buf: string[] = [];
        s.on('error', (e) => reject(e));
        s.on('data', (chunk) => {
            if (chunk instanceof Buffer) {
                buf.push(chunk.toString(encoding));
            } else {
                buf.push(chunk);
            }
        });
        s.on('end', () => resolve(buf.join('')));
    });
}
