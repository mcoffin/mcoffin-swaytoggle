import _ from 'lodash';
import minimist from 'minimist';

export class RequiredArgumentError extends Error {
    key: string;
    constructor(key: string) {
        super(`missing required argument: ${key}`);
        this.key = key;
    }
}

export class MinimistConfig {
    minimistConfig: minimist.Opts;
    argv: string[];
    config: minimist.ParsedArgs;

    constructor(args?: string[], minimistConfig?: minimist.Opts) {
        this.minimistConfig = minimistConfig || {};
        this.argv = args || process.argv.slice(2);
        this.config = minimist(this.argv, this.minimistConfig);
    }

    get<T>(key: string, defaultValue?: T): T | undefined {
        return this.config[key] || defaultValue;
    }

    getRequired<T>(key: string): T {
        const ret = this.get<T>(key);
        if (ret === undefined) {
            throw new RequiredArgumentError(key);
        }
        return ret;
    }

    get trailing(): string[] {
        const beforeDash = this.config._;
        const afterDash = this.config['--'] || [];
        return beforeDash.concat(afterDash);
    }
}
