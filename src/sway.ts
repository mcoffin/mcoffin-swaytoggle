import { spawn } from 'child_process';
import { awaitTerminationChecked, runSimpleCommand } from './process-utils';
import { streamToString } from './stream-utils';

export interface Output {
    id: number;
    name: string;
    active: boolean;
}

export async function getOutputs(): Promise<Output[]> {
    const child = spawn('swaymsg', ['-r', '-t', 'get_outputs'], { stdio: ['ignore', 'pipe', 'inherit'] });
    const readOutput = streamToString(child.stdout);
    await awaitTerminationChecked(child, 'swaymsg');
    return await readOutput.then(JSON.parse);
}

export async function setOutputEnabled(output: string, enabled: boolean): Promise<void> {
    await runSimpleCommand('swaymsg', ['output', output, enabled ? 'enable' : 'disable']);
}
