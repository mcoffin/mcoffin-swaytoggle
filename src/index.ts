import _ from 'lodash';
import { WofiOptions, wofi, chooser } from './wofi';
import * as sway from './sway';
import { Output } from './sway';
import { MinimistConfig } from './config';
import { spawn } from 'child_process';
import { awaitTerminationChecked } from './process-utils';
import { streamToString } from './stream-utils';

class Config extends MinimistConfig {
    constructor(args?: string[]) {
        super(args, {
            string: [
                'output',
                'wofi-conf',
                'wofi-style',
                'chooser-command',
            ],
            boolean: [
                'slurp',
            ],
        });
    }

    get wofiOptions(): WofiOptions {
        return {
            conf: this.get<string>('wofi-conf'),
            style: this.get<string>('wofi-style'),
        };
    }

    get chooserCommand(): string | undefined {
        return this.get<string>('chooser-command');
    }

    async output(availableOutputs: string[]): Promise<string | undefined> {
        let ret = this.get<string>('output');
        if (ret) {
            return ret;
        }
        try {
            if (this.get('slurp', false)) {
                const slurp = spawn('slurp', ['-f', '%o', '-or'], { stdio: ['ignore', 'pipe', 'inherit'] });
                const readOutput = streamToString(slurp.stdout);
                await awaitTerminationChecked(slurp);
                ret = await readOutput;
            } else if (this.chooserCommand) {
                ret = await chooser(this.chooserCommand, availableOutputs);
            } else {
                ret = await wofi(availableOutputs, { ...this.wofiOptions, prompt: 'Choose an output to toggle' })
            }
        } catch(e) {
            ret = '';
        }
        if (ret.length < 1) {
            return;
        }
        return ret;
    }
}

function toRecordWithKey<T>(values: T[], key: string | ((v: T) => string)): Record<string, T> {
    const getKey = _.isFunction(key) ? key : _.property(key);
    return Object.fromEntries(values.map((v) => [getKey(v), v]));
}

async function main(config: Config): Promise<any> {
    const availableOutputs = await sway.getOutputs()
        .then((outputs) => toRecordWithKey(outputs, (output) => output.name));
    const output = await config.output(Object.keys(availableOutputs));
    if (!output) {
        throw new Error('no output chosen');
    }
    await sway.setOutputEnabled(output, !availableOutputs[output]?.active);
}

main(new Config(process.argv.slice(2)))
    .catch((e) => {
        console.error(`${e}`);
        process.exit(1);
    });
